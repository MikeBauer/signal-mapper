package com.bauerproductions.signalmapper.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Michael on 19/07/2016.
 */
public class WebAdapter {

    private static String defaultEndpoint = "http://mikebauer.pythonanywhere.com/";
    //private static String defaultEndpoint = "http://124.190.178.41/";

    private static WebAdapter mInstance;

    public Context context;
    public String endpoint;

    public static ArrayList<HashMap<String,String>> cache =  new ArrayList<HashMap<String,String>>();
    //private static AsyncHttpClient client = new AsyncHttpClient();

    private WebAdapter(Context context){
        this.context = context;
        this.endpoint = defaultEndpoint;
    }

    public static WebAdapter getInstance(Context context){
        if(mInstance==null){
            mInstance = new WebAdapter(context);
        } else {
            mInstance.context = context;
        }
        return mInstance;
    }

    public void setEndpoint(String endpoint){
        this.endpoint = endpoint;
    }

    public void testInternet() throws IOException {

        final String endpoint = this.endpoint;

        Request test = new Request(endpoint, "POST", new QuickResponse() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(context,"Internet Success: "+endpoint,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String response) {
                Toast.makeText(context,"Internet Fail: "+endpoint,Toast.LENGTH_SHORT).show();
            }
        });
        test.execute();
    }


    public void getListOfTags(final QuickResponse callback){
        final String endpoint = this.endpoint;

        Request post = new Request(endpoint + "api/taglist", "POST", new QuickResponse() {
            @Override
            public void onSuccess(String response) {
                String resp = new String(response);
                if (resp.startsWith("fail") || resp.equals("")) {
                    if (callback != null)
                        callback.onFailure(resp);
                    Log.d("SignalMapper","Sent data to "+endpoint+"api/taglist but something went wrong: response = "+resp);
                } else {
                    if (callback != null)
                        callback.onSuccess(resp);
                    Log.d("SignalMapper","got data successfully from "+endpoint+"api/taglist");
                }
            }

            @Override
            public void onFailure(String response) {
                if(callback!=null){
                    callback.onFailure("Failed to send data!");
                    Log.d("SignalMapper","Failed to send data to "+endpoint+"api/add");
                }
            }
        });
        post.execute();

    }


    public void sendSingleDataPoint(DataPoint data,QuickResponse callback){
        sendSingleDataPoint(data.toMap(),callback);
    }

    public void sendSingleDataPoint(HashMap<String,String> data,final QuickResponse callback){
        //Send post request here

        final String endpoint = this.endpoint;

        Request post = new Request(endpoint+"api/add", "POST", new QuickResponse() {
            @Override
            public void onSuccess(String response) {
                //We can maybe do something here, not sure if anything needed
                String resp = new String(response);
                if (resp.startsWith("fail")) {
                    if (callback != null)
                        callback.onFailure(resp);
                    Log.d("SignalMapper","Sent data to "+endpoint+"api/add but something went wrong: response = "+resp);
                } else {
                    if (callback != null)
                        callback.onSuccess(resp);
                    Log.d("SignalMapper","got data successfully from "+endpoint+"api/add");
                }
            }

            @Override
            public void onFailure(String response) {
                if(callback!=null){
                    callback.onFailure("Failed to send data!");
                    Log.d("SignalMapper","Failed to send data to "+endpoint+"api/add");
                }
            }
        });
        post.execute(data);
    }

    public void sendMany(ArrayList<DataPoint> data,QuickResponse callback,ProgressCallback progress){
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String, String>>();
        for(DataPoint d : data){
            res.add(d.toMap());
        }
        sendManyDataPoints(res,callback,progress);
    }

    public void sendManyDataPoints(ArrayList<HashMap<String,String>> data,final QuickResponse callback,final ProgressCallback progress){
        JSONUtils.toJSONStringAsync(data, new JSONUtils.JSONPackerCallback() {
            @Override
            public void onFinish(String json) {
                sendManyDataPoints(json, callback);
            }

            @Override
            public void onProgress(int percent) {
                if(progress!=null)
                    progress.onProgress(percent);
            }
        });
    }

    public void sendManyDataPoints(String json,final QuickResponse callback){

        final String endpoint = this.endpoint;
        HashMap<String,String> postData = new HashMap<String,String>();
        HashMap<String,String> extraData = new HashMap<String,String>();
        extraData.put("json",json);

        Request post = new Request(endpoint+"api/json", "POST", new QuickResponse() {
            @Override
            public void onSuccess(String response) {
                //We can maybe do something here, not sure if anything needed
                String resp = new String(response);
                if (resp.startsWith("fail") || resp.equals("")) {
                    if (callback != null)
                        callback.onFailure(resp);
                    Log.d("SignalMapper","Sent data to "+endpoint+"api/json but something went wrong: response = "+resp);
                } else {
                    if (callback != null)
                        callback.onSuccess(resp);
                    Log.d("SignalMapper","got data successfully from "+endpoint+"api/json");
                }
            }

            @Override
            public void onFailure(String response) {
                if(callback!=null){
                    callback.onFailure("Failed to send data!");
                    Log.d("SignalMapper","Failed to send data to "+endpoint+"api/json");
                }
            }
        });
        post.execute(postData,extraData);

    }

    private class Request extends AsyncTask<HashMap<String,String>, Integer, String> {

        private int status = -1;
        public int connectTimeout = 5000;
        public int readTimeout = 30000;

        URL url;
        String method;
        QuickResponse callback;
        boolean done;

        public Request(String url,String method,QuickResponse callback){
            super();
            try {
                this.url = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            this.method = method;
            this.done = false;
            this.callback = callback;
        }

        public int getResponseStatus(){
            if(done){
                return status;
            }
            return -1;
        }

        private String getQuery(HashMap<String,String> params) throws UnsupportedEncodingException
        {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }

        protected String doInBackground(HashMap<String,String>... data) {
            HttpURLConnection conn  = null;
            String response = "";
            try {
                conn = (HttpURLConnection) this.url.openConnection();
                conn.setConnectTimeout(connectTimeout);
                conn.setReadTimeout(readTimeout);
                conn.setRequestMethod(method);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                if(data.length>1 && data[1].get("json") != null)
                    conn.setRequestProperty("Content-Type", "application/json");

                if(data.length>0) {
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getQuery(data[0]));
                    if(data.length>1){
                        String json = data[1].get("json");
                        if(json!=null){
                            writer.write(json);
                        }
                    }
                    writer.flush();
                    writer.close();
                    os.close();
                }
                conn.connect();
                status = conn.getResponseCode();
                if (status == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                }
                else {
                    response="UNEXPECTEDRESPONSECODE: status: "+status;
                }
                return response;
            } catch(MalformedURLException error) {
                //Handles an incorrectly entered URL
                return "MalformedURLException";
            }
            catch(SocketTimeoutException error) {
                //Handles URL access timeout.
                return "SocketTimeoutException";
            }
            catch (IOException error) {
                //Handles input and output errors
                return "IOException";
            }finally {
                if(conn != null) // Make sure the connection is not null.
                    conn.disconnect();
                return response;
            }
        }

        protected void onPostExecute(String result) {
            if(result!=null)
                if(result.equals("MalformedURLException"))
                    callback.onFailure(result);
                else if(result.equals("SocketTimeoutException"))
                    callback.onFailure(result);
                else if(result.equals("IOException"))
                    callback.onFailure(result);
                else if(result.startsWith("UNEXPECTED")){
                    callback.onFailure(result);
                }
                else
                    callback.onSuccess(result);

        }
    }

    public interface QuickResponse{
        void onSuccess(String response);
        void onFailure(String response);
    }

    public interface ProgressCallback{
        void onProgress(int percent);
    }
}
