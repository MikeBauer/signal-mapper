package com.bauerproductions.signalmapper;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bauerproductions.signalmapper.utils.Cache;
import com.bauerproductions.signalmapper.utils.WebAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnCacheFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CacheFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CacheFragment extends Fragment {
    private OnCacheFragmentInteractionListener mListener;

    private TextView cachePreview;
    private TextView cacheSize;

    public CacheFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AboutFragment.
     */
    public static CacheFragment newInstance(String param1, String param2) {
        CacheFragment fragment = new CacheFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View frag = inflater.inflate(R.layout.content_cache, container, false);

        cachePreview = (TextView) frag.findViewById(R.id.textViewCachePreview);
        cacheSize = (TextView) frag.findViewById(R.id.textViewCacheNumDataPoints);

        Button clearCache = (Button)frag.findViewById(R.id.buttonClearCache);
        clearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to delete the Cache?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Cache.clearCache();
                                updateUI();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                builder.create().show();
            }
        });

        final Button preview = (Button) frag.findViewById(R.id.buttonPreviewCache);
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = Cache.size();
                if(size>1000){
                    Toast.makeText(getContext(),"This may take a while... processing "+size+" datapoints.",Toast.LENGTH_LONG).show();
                }
                Cache.printAsync(new Cache.PrintCallback() {
                    @Override
                    public void onFinish(String result) {
                        cachePreview.setText(result);
                    }

                    @Override
                    public void onProgress(int percent) {
                        cachePreview.setText("Loading... "+percent+"%");
                    }
                });
            }
        });

        Button send = (Button) frag.findViewById(R.id.buttonSendCachedData);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = Cache.size();
                if(size>1000){
                    Toast.makeText(getContext(),"This may take a while... processing "+size+" datapoints.",Toast.LENGTH_LONG).show();
                }
                cachePreview.setText("Sending Data... Please Wait...");
                sendCachedData();

            }
        });

        Button sendChunks = (Button) frag.findViewById(R.id.buttonSendCachedDataAsChunks);
        sendChunks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = Cache.size();
                if(size>1000){
                    Toast.makeText(getContext(),"This may take a while... processing "+size+" datapoints.",Toast.LENGTH_LONG).show();
                }
                cachePreview.setText("Sending Data as "+((size/Cache.chunkSize)+1)+" Chunks... Please Wait...");
                sendCachedDataAsChunks();

            }
        });

        return frag;
    }


    public void sendCachedDataAsChunks(){
        final WebAdapter webAdapter = WebAdapter.getInstance(getContext());
        Cache.sendCacheByChunksToServer(webAdapter, new WebAdapter.QuickResponse() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(webAdapter.context, "Success: " + response, Toast.LENGTH_SHORT).show();
                cachePreview.setText("Data Successfully Sent to Server.\nServer Response:\n"+response);
                updateUI();
            }

            @Override
            public void onFailure(String response) {
                Toast.makeText(webAdapter.context, "Failure: " + response, Toast.LENGTH_SHORT).show();
                cachePreview.setText(response);
            }
        }, new WebAdapter.ProgressCallback() {
            @Override
            public void onProgress(int percent) {
                if(percent>=99){
                    cachePreview.setText("Sending Chunks... "+percent+"%\nAll Chunks Sent...");
                } else {
                    cachePreview.setText("Sending Chunks... "+percent+"%");
                }
            }
        });
    }

    public void sendCachedData(){
        final WebAdapter webAdapter = WebAdapter.getInstance(getContext());
        Cache.sendCacheToServer(webAdapter, new WebAdapter.QuickResponse() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(webAdapter.context, "Success: " + response, Toast.LENGTH_SHORT).show();
                cachePreview.setText("Data Successfully Sent to Server.\nServer Response:\n"+response);
                Cache.clearCache();
                updateUI();
            }

            @Override
            public void onFailure(String response) {
                Toast.makeText(webAdapter.context, "Failure: " + response, Toast.LENGTH_SHORT).show();
                cachePreview.setText(response);
            }
        }, new WebAdapter.ProgressCallback() {
            @Override
            public void onProgress(int percent) {
                if(percent>=99){
                    cachePreview.setText("Packing Data into JSON... "+percent+"%\nSending Data to Server...");
                } else {
                    cachePreview.setText("Packing Data into JSON... "+percent+"%");
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    public void updateUI(){
        cacheSize.setText("DataPoints:\t\t"+Cache.size());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onCacheFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCacheFragmentInteractionListener) {
            mListener = (OnCacheFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChartsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCacheFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCacheFragmentInteraction(Uri uri);
    }
}
