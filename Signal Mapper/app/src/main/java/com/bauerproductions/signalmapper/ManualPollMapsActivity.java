package com.bauerproductions.signalmapper;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bauerproductions.signalmapper.utils.Cache;
import com.bauerproductions.signalmapper.utils.DataPoint;
import com.bauerproductions.signalmapper.utils.LocationManager;
import com.bauerproductions.signalmapper.utils.WebAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

public class ManualPollMapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,GoogleMap.OnMapLongClickListener, LocationListener {

    private final String TAG = "manualpollmapsactivity";
    private TextView cacheSize;
    private TextView dataPointText;
    private TextView uploadProgress;
    private Button pollButton;
    private GoogleMap mMap;
    private Circle userLocation;
    private Circle gpsAccuracy;
    private String tag="";

    private boolean currentlyUploading = false;
    private boolean modeAuto = false;
    private boolean paused = false;
    private int minAccuracy = 0; //0 means no limit

    private double latitude=0.0;
    private double longitude=0.0;
    private double accuracy=0.0;
    private double altitude=0.0;
    private double bearing=0.0;
    private double speed=0.0;
    private boolean userOverride=false;
    private double longitudeUser=0.0;
    private double latitudeUser=0.0;
    private double accuracyUser=0.0;
    private double altitudeUser=0.0;
    private double bearingUser=0.0;
    private double speedUser=0.0;

    private TextView latlongText;
    private int collectedDataPoints=0;

    //size of world is  256*2^defaultZoom dp
    private float defaultZoom = 18f;

    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_poll_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.poll_map);
        mapFragment.getMapAsync(this);

        latlongText = (TextView)findViewById(R.id.textViewLatLngPoint);
        cacheSize = (TextView) findViewById(R.id.textViewCacheSize);
        dataPointText = (TextView) findViewById(R.id.textViewDataPoints);
        uploadProgress = (TextView) findViewById(R.id.textViewUploadProgress);
        pollButton = (Button) findViewById(R.id.buttonPing);

        Bundle extras = this.getIntent().getExtras();

        minAccuracy = extras.getInt(MainActivity.EXTRA_POLL_ACCURACY,0);

        tag = extras.getString(MainActivity.EXTRA_POLL_TAG);
        modeAuto = false;
        if(extras.getInt(MainActivity.EXTRA_POLL_MODE,R.id.radioButtonManual)==R.id.radioButtonAuto){
            modeAuto = true;
            pollButton.setText("Pause");
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        collectedDataPoints = Cache.size();
        cacheSize.setText("Tag: "+tag+"   Cache: "+collectedDataPoints);
    }

    @Override
    protected void onStop() {
        LocationManager.getInstance(this).stopManualPolling();
        super.onStop();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-33.7749,151.1155),defaultZoom));
        LocationManager lm = LocationManager.getInstance(this);
        lm.setLocationListener(this);
        lm.startManualPolling();
    }


    public void onPollClick(View view){
        if(modeAuto){
            //Act as a pause/resume button
            paused = !paused;
            if(paused){
                pollButton.setText("Resume");
            } else {
                pollButton.setText("Pause");
            }
        } else {
            //poll networks
            pollNetworks();
        }
    }


    public void pollNetworks(){
        Location loc = new Location("manual");
        if (userOverride) {
            loc.setAccuracy((float) accuracyUser);
            loc.setLongitude(longitudeUser);
            loc.setLatitude(latitudeUser);
            loc.setBearing((float) bearingUser);
            loc.setSpeed((float) speedUser);
            loc.setAltitude((float) altitudeUser);
        } else {
            loc.setAccuracy((float) accuracy);
            loc.setLongitude(longitude);
            loc.setLatitude(latitude);
            loc.setBearing((float) bearing);
            loc.setSpeed((float) speed);
            loc.setAltitude((float) altitude);
        }

        int points = DataPoint.pollNetwork(this, tag, loc);
        collectedDataPoints += points;
        cacheSize.setText("Tag: " + tag + "   Cache: " + collectedDataPoints);
        dataPointText.setText("Retrieved " + points + " datapoints");

        //Now if autoChunkUpload is true, we have more than a chunk's worth of data
        //and not currently uploading then compress and send a chunk
        if(Cache.autoChunkUpload && collectedDataPoints>=Cache.chunkSize && !currentlyUploading){
            Cache.sendChunkToServer(WebAdapter.getInstance(this), new WebAdapter.QuickResponse() {
                @Override
                public void onSuccess(String response) {
                    uploadProgress.setText("Upload Success.");
                    collectedDataPoints = Cache.size();
                    cacheSize.setText("Tag: " + tag + "   Cache: " + collectedDataPoints);
                    currentlyUploading = false;
                }

                @Override
                public void onFailure(String response) {
                    uploadProgress.setText("Upload Failure.");
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Chunk Upload failed! Please check your connection status.");
                    builder.setPositiveButton("Retry Upload",null);
                    builder.setNegativeButton("Cache Only", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Cache.autoChunkUpload = false;
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    currentlyUploading = false;
                    uploadProgress.setText("");
                }
            }, new WebAdapter.ProgressCallback() {
                @Override
                public void onProgress(int percent) {
                    if(percent>=99){
                        uploadProgress.setText("Sending Chunk to Server...");
                    } else {
                        uploadProgress.setText("Building Chunk: "+percent+"%");
                    }

                }
            });
            currentlyUploading = true;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG,"ManualPollMapsActivity.onLocationChanged Called");
        LocationManager locationManager = LocationManager.getInstance(this);
        LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        accuracy = location.getAccuracy();
        bearing = location.getBearing();
        speed = location.getSpeed();
        altitude = location.getAltitude();
        if(!userOverride){
            latlongText.setText("Lat: "+latitude+"\nLng: "+longitude);
        }
        if(gpsAccuracy==null){
            gpsAccuracy = mMap.addCircle(new CircleOptions()
                    .center(pos)
                    .radius(location.getAccuracy())
                    .strokeColor(Color.BLACK)
                    .strokeWidth(5F)
            );
        } else {
            gpsAccuracy.setCenter(pos);
            gpsAccuracy.setRadius(location.getAccuracy());
        }
        if(userLocation==null){
            userLocation = mMap.addCircle(new CircleOptions()
                    .center(pos)
                    .radius(1f)
                    .strokeColor(Color.BLACK)
                    .strokeWidth(5F)
            );
        } else {
            userLocation.setCenter(pos);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));
        if(modeAuto && !paused && (accuracy <= minAccuracy || minAccuracy==0)){
            //poll networks
            pollNetworks();
        }
    }

    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.v(TAG, "Status changed: " + s);
    }

    public void onProviderEnabled(String s) {
        Log.e(TAG, "PROVIDER DISABLED: " + s);
    }

    public void onProviderDisabled(String s) {
        Log.e(TAG, "PROVIDER DISABLED: " + s);
    }


    @Override
    public void onMapClick(LatLng latLng) {
        if(userOverride){
            latitudeUser = latLng.latitude;
            longitudeUser = latLng.longitude;
            latlongText.setText("Lat: "+latitudeUser+"\nLng: "+longitudeUser);
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        latitudeUser = latLng.latitude;
        longitudeUser = latLng.longitude;
        userOverride = !userOverride;
        if(userOverride){
            Toast.makeText(this,"Overriding GPS location",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"Resuming GPS location",Toast.LENGTH_SHORT).show();
        }
        latlongText.setText("Lat: "+latitudeUser+"\nLng: "+longitudeUser);
    }
}
