package com.bauerproductions.signalmapper;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bauerproductions.signalmapper.utils.JSONUtils;
import com.bauerproductions.signalmapper.utils.NetworkManager;
import com.bauerproductions.signalmapper.utils.WebAdapter;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnManualPollFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ManualPollFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManualPollFragment extends Fragment{

    ListView tagList;
    TextView status;
    EditText customTag;
    EditText minAccuracy;
    RadioGroup whichMode;

    boolean loading = false;

    ArrayList<HashMap<String,String>> cachedTagList;
    String selectedTag="";

    private OnManualPollFragmentInteractionListener mListener;

    private final WebAdapter.QuickResponse callback = new WebAdapter.QuickResponse() {
        @Override
        public void onSuccess(String response) {
            if(status != null && tagList != null){
                cachedTagList = JSONUtils.fromJSONString(response);
                if(cachedTagList!=null) {
                    ListAdapter adapter = new ListAdapter(getContext(), R.layout.list_view_tags, cachedTagList);
                    tagList.setAdapter(adapter);
                    status.setVisibility(View.GONE);
                } else {
                    status.setText("Cannot Load Tags from Server, please check your connection and tap this message to retry!\nResponse: "+response);
                }
            }
        }

        @Override
        public void onFailure(String response) {
            if(status != null)
                status.setText("Cannot Load Tags from Server, please check your connection and tap this message to retry!\nResponse: "+response);
        }
    };

    public ManualPollFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManualPollFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManualPollFragment newInstance(String param1, String param2) {
        ManualPollFragment fragment = new ManualPollFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View frag = inflater.inflate(R.layout.content_manual_poll, container, false);

        //Get available networks
        NetworkManager networkManager = NetworkManager.getInstance(getContext());

        TextView networkList = (TextView) frag.findViewById(R.id.textViewNetworkList);
        String list = "";
        if(networkManager.hasWIFIConnection()){
            list = list+"WIFI   ";
        }
        String tele = networkManager.getTelephonyConnection().name();
        if(!tele.equals("UNKNOWN")){
            list = list+tele+"   ";
        }
        networkList.setText(list);

        whichMode = (RadioGroup) frag.findViewById(R.id.radioGroupMode);
        minAccuracy = (EditText) frag.findViewById(R.id.editTextMinAcc);

        //Make start button work
        Button start = (Button) frag.findViewById(R.id.open_poll_map);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startManualPoll();
            }
        });

        //Handle Tag selection
        customTag = (EditText) frag.findViewById(R.id.manual_poll_text_tag);
        tagList = (ListView) frag.findViewById(R.id.listViewTag);
        tagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedTag = cachedTagList.get(position).get("tag");
                customTag.setText(selectedTag);
            }
        });

        status = (TextView) frag.findViewById(R.id.textViewLoadingTagList);
        status.setVisibility(View.VISIBLE);
        status.setText("Loading List...");
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!loading){
                    WebAdapter webAdapter = WebAdapter.getInstance(getContext());
                    loading = true;
                    status.setText("Loading List...");
                    webAdapter.getListOfTags(callback);
                }
            }
        });

        WebAdapter webAdapter = WebAdapter.getInstance(getContext());
        loading = true;
        webAdapter.getListOfTags(callback);

        return frag;
    }

    public void startManualPoll(){
        Intent intent = new Intent(getContext(), ManualPollMapsActivity.class);
        if(customTag.getText().toString().length()>0){
            intent.putExtra(MainActivity.EXTRA_POLL_TAG,customTag.getText().toString());
        } else {
            intent.putExtra(MainActivity.EXTRA_POLL_TAG,selectedTag);
        }
        intent.putExtra(MainActivity.EXTRA_POLL_MODE,whichMode.getCheckedRadioButtonId());
        try{
            intent.putExtra(MainActivity.EXTRA_POLL_ACCURACY,Integer.parseInt(minAccuracy.getText().toString()));
        } catch(NumberFormatException e){
            intent.putExtra(MainActivity.EXTRA_POLL_ACCURACY,0);
        }
        startActivity(intent);
    }

    public void onButtonPressed(View view) {
        if (mListener != null) {
            mListener.onManualPollFragmentInteraction(view);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnManualPollFragmentInteractionListener) {
            mListener = (OnManualPollFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChartsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnManualPollFragmentInteractionListener {
        // TODO: Update argument type and name
        void onManualPollFragmentInteraction(View view);
    }
}
