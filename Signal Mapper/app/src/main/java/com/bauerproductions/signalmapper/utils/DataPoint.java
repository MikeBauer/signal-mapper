package com.bauerproductions.signalmapper.utils;

import android.content.Context;
import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Michael on 18/08/2016.
 *
 * Container class to hold measurements before uploading.
 */
public class DataPoint {

    int id;
    String timestamp;
    double longitude;
    double latitude;
    double accuracy;
    double altitude;
    double speed;
    double bearing;
    int network_type;
    int network_strength;
    String ssid;
    String bssid;
    String cid;
    String tag;

    public DataPoint(double longitude, double latitude, double accuracy,double altitude,
                     double speed,double bearing, int network_type, int network_strength,
                     String ssid,String bssid,String cid, String tag){
        this.id = -1;
        this.timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        this.longitude = longitude;
        this.latitude = latitude;
        this.accuracy = accuracy;
        this.altitude = altitude;
        this.speed = speed;
        this.bearing = bearing;
        this.network_type = network_type;
        this.network_strength = network_strength;
        this.ssid = ssid;
        this.bssid = bssid;
        this.cid = cid;
        this.tag = tag;
    }

    public DataPoint(HashMap<String,String> map){
        if(map.get("id")!=null)
            this.id = Integer.parseInt(map.get("id"));
        else
            this.id = -1;
        this.timestamp = map.get("timestamp");
        if(this.timestamp == null)
            this.timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        this.longitude = Double.parseDouble(map.get("longitude"));
        this.latitude = Double.parseDouble(map.get("latitude"));
        this.accuracy = Double.parseDouble(map.get("accuracy"));
        this.altitude = Double.parseDouble(map.get("altitude"));
        this.speed = Double.parseDouble(map.get("speed"));
        this.bearing = Double.parseDouble(map.get("bearing"));
        this.network_type = NetworkManager.Network_Type.valueOf(map.get("network_type")).ordinal();
        this.network_strength = Integer.parseInt(map.get("network_strength"));
        this.ssid = map.get("ssid");
        this.bssid = map.get("bssid");
        this.cid = map.get("cid");
        this.tag = map.get("tag");
    }

    public HashMap<String,String> toMap(){
        HashMap<String,String> result = new HashMap<String,String>();
        if(id>=0)
            result.put("id",String.valueOf(id));
        result.put("timestamp",timestamp);
        result.put("longitude",String.valueOf(longitude));
        result.put("latitude",String.valueOf(latitude));
        result.put("accuracy",String.valueOf(accuracy));
        result.put("altitude",String.valueOf(altitude));
        result.put("speed",String.valueOf(speed));
        result.put("bearing",String.valueOf(bearing));
        result.put("network_type",String.valueOf(network_type));
        result.put("network_strength",String.valueOf(network_strength));
        result.put("ssid",ssid);
        result.put("bssid",bssid);
        result.put("cid",cid);
        result.put("tag",tag);
        return result;
    }

    public static int pollNetworkAutoGetLocation(Context context,String tag){
        LocationManager locationManager = LocationManager.getInstance(context);
        return pollNetwork(context,tag,locationManager.getCurrentLocation());
    }

    public static int pollNetwork(Context context,String tag,Location location){
        NetworkManager networkManager = NetworkManager.getInstance(context);
        //assign variable space now
        ArrayList<HashMap<String,String>> tempMapping;
        HashMap<String,String> newDP;
        ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();

        //get WIFI
        tempMapping = networkManager.pollAllWIFINetworks();

        for(HashMap<String,String> networkData: tempMapping){
            newDP = new HashMap<String,String>();
            newDP.put("tag",tag);
            newDP.put("latitude",String.valueOf(location.getLatitude()));
            newDP.put("longitude",String.valueOf(location.getLongitude()));
            newDP.put("accuracy",String.valueOf(location.getAccuracy()));
            newDP.put("altitude",String.valueOf(location.getAltitude()));
            newDP.put("speed",String.valueOf(location.getSpeed()));
            newDP.put("bearing",String.valueOf(location.getBearing()));
            newDP.put("network_type",String.valueOf(NetworkManager.Network_Type.WIFI));
            newDP.put("network_strength",networkData.get("level"));
            newDP.put("ssid",networkData.get("ssid"));
            newDP.put("bssid",networkData.get("bssid"));
            newDP.put("cid","");

            DataPoint dp = new DataPoint(newDP);
            dataPoints.add(dp);
            Cache.addDatapoint(dp);
        }

        //Get Mobile Networks
        tempMapping = networkManager.pollAllMobileNetworks();

        for(HashMap<String,String> networkData: tempMapping){
            newDP = new HashMap<String,String>();
            newDP.put("tag",tag);
            newDP.put("latitude",String.valueOf(location.getLatitude()));
            newDP.put("longitude",String.valueOf(location.getLongitude()));
            newDP.put("accuracy",String.valueOf(location.getAccuracy()));
            newDP.put("altitude",String.valueOf(location.getAltitude()));
            newDP.put("speed",String.valueOf(location.getSpeed()));
            newDP.put("bearing",String.valueOf(location.getBearing()));
            newDP.put("network_type",networkData.get("type"));
            newDP.put("network_strength",networkData.get("level"));
            newDP.put("ssid","");
            newDP.put("bssid","");
            newDP.put("cid",networkData.get("id"));

            DataPoint dp = new DataPoint(newDP);
            dataPoints.add(dp);
            Cache.addDatapoint(dp);
        }

        return dataPoints.size();
    }


}