package com.bauerproductions.signalmapper.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Michael on 23/08/2016.
 */
public class Cache {

    public final static String PREFERENCES_NAME = "com.bauerproductions.cache";
    public final static String CACHE_NAME = "cache";

    public static boolean attemptedLoadFromMemory = false;
    public static int chunkSize = 1000;

    //it for uploading chunks while collecting data
    public static boolean autoChunkUpload = true;

    private static ArrayList<HashMap<String,String>> data = new ArrayList<HashMap<String, String>>();

    private static boolean sending = false;

    //Sending all cache chunks
    private static ArrayList<HashMap<String,String>> tmpChunk = new ArrayList<HashMap<String, String>>();
    private static WebAdapter.QuickResponse loopCallback;
    private static WebAdapter loopWebAdapter;
    private static WebAdapter.ProgressCallback loopProgress;
    private static int chucksToSend = 0;
    private static int count = 0;
    private static final WebAdapter.QuickResponse loop = new WebAdapter.QuickResponse() {
        @Override
        public void onSuccess(String response) {
            if(data.size()>0){
                sendChunkToServer(loopWebAdapter,loop,null);
                loopProgress.onProgress((int)(((float)count/(float)chucksToSend)*100));
                count++;
            } else {
                //success, cleanup this 'loop'
                tmpChunk = new ArrayList<HashMap<String, String>>();
                loopCallback.onSuccess(response);
                sending = false;
            }
        }

        @Override
        public void onFailure(String response) {
            //abort, but first add the temp chunk back to the data
            data.addAll(tmpChunk);
            loopCallback.onFailure(response);
            sending = false;
        }
    };

    public static void clearCache(){
        data = new ArrayList<HashMap<String, String>>();
    }

    public static int size(){
        return data.size();
    }

    public static String print(){
        String result = "";
        for(HashMap<String,String> map : data){
            Iterator it = ((HashMap<String,String>)map.clone()).entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<String,String> pair = (Map.Entry<String,String>) it.next();
                result = result+pair.getKey()+" \t\t "+pair.getValue()+"\n";
                it.remove();
            }
            result = result + "---------\n";
        }
        return result;
    }

    public static  void printAsync(PrintCallback callback){
        CacheAsyncPrinter task = new CacheAsyncPrinter(callback);
        task.execute(data);
    }

    public static boolean loadFromMemory(Context context){
        if(!attemptedLoadFromMemory) {
            SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            data = JSONUtils.fromJSONString(sharedPref.getString(CACHE_NAME, null));
            if (data == null)
                data = new ArrayList<HashMap<String, String>>();
            attemptedLoadFromMemory = true;
            return data != null || data.size() == 0;
        }
        return false;
    }

    public static void loadToMemory(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(CACHE_NAME,JSONUtils.toJSONString(data));
        editor.commit();
        attemptedLoadFromMemory = false;
        clearCache();
    }

    public static void addDatapoint(DataPoint dataPoint){
        data.add(dataPoint.toMap());
    }

    public static void sendCacheToServer(WebAdapter webAdapter, WebAdapter.QuickResponse callback,WebAdapter.ProgressCallback progress){
        webAdapter.sendManyDataPoints(data,callback,progress);
    }

    public static void sendChunkToServer(WebAdapter webAdapter, WebAdapter.QuickResponse callback,WebAdapter.ProgressCallback progress){
        tmpChunk = new ArrayList<HashMap<String, String>>();
        int chunkSize = Cache.chunkSize;
        if(data.size()<chunkSize)
            chunkSize = data.size();
        for(int i=chunkSize-1;i>=0;i--){
            tmpChunk.add(data.remove(i));
        }
        webAdapter.sendManyDataPoints(tmpChunk,callback,progress);
    }

    public static void sendCacheByChunksToServer(final WebAdapter webAdapter, final WebAdapter.QuickResponse callback, WebAdapter.ProgressCallback progress){
        sending = true;
        loopWebAdapter = webAdapter;
        loopCallback = callback;
        loopProgress = progress;
        chucksToSend = ((data.size()/Cache.chunkSize)+1);
        count = 0;
        loop.onSuccess(null);
    }

    private static class CacheAsyncPrinter extends AsyncTask<ArrayList<HashMap<String,String>>, Integer, String> {

        PrintCallback callback;
        int percent=0;

        public CacheAsyncPrinter(PrintCallback callback){
            this.callback = callback;
        }

        protected String doInBackground(ArrayList<HashMap<String,String>>... data) {

            if(data.length>0){
                ArrayList<HashMap<String,String>> toProcess = data[0];
                String result = "-----------\n";
                int size = toProcess.size();
                for(int i=0;i<size;i++){
                    HashMap<String,String> map = toProcess.get(i);
                    Iterator it = ((HashMap<String,String>)map.clone()).entrySet().iterator();
                    while(it.hasNext()){
                        Map.Entry<String,String> pair = (Map.Entry<String,String>) it.next();
                        result = result+pair.getKey()+" \t\t "+pair.getValue()+"\n";
                        it.remove();
                    }
                    result = result + "-----------\n";
                    //Because it will be common to send hundreds or thousands of datapoints
                    //It's best not to have the progress called thousands of times. At most 100 times.
                    int percentNow = (int)((i/ (float)size)*100);
                    if(percentNow>percent){
                        percent = percentNow;
                        publishProgress(percent);
                    }
                }
                return result;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            callback.onProgress(values[0]);
        }

        protected void onPostExecute(String result) {
            callback.onFinish(result);
        }
    }

    public interface PrintCallback{
        void onFinish(String result);
        void onProgress(int percent);
    }
}
