package com.bauerproductions.signalmapper.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Michael on 11/07/2016.
 */
public class LocationManager implements LocationListener {

    private static LocationManager mInstance;

    private final String TAG = "locationmanager";

    public String mLastUpdateTime;

    private boolean isManualPolling = false;
    private boolean isAutoPolling = false;
    private boolean isUpdatingLocation = false;
    private Location mLastKnownLocation;
    private Context context;
    private LocationListener manualPollLocationListener;
    private android.location.LocationManager lm;
    private String provider="gps";

    public LocationManager() {
    }

    public static LocationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LocationManager();
        }
        mInstance.context = context;
        return mInstance;
    }

    public void setLocationListener(LocationListener ll) {
        this.manualPollLocationListener = ll;
    }

    public Location getLastKnownLocation() {
        return mLastKnownLocation;
    }

    public Location getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //No permission to grab user location data, should probably warn the user.
            Toast.makeText(context, "You haven't granted this app permission to get location data", Toast.LENGTH_LONG);
            return null;
        }
        lm = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLastKnownLocation = lm.getLastKnownLocation(provider);
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        return mLastKnownLocation;

    }

    public void onLocationChanged(Location location) {
        Log.d(TAG, "LocationManager.onLocationChanged Called");
        if(location==null)
            return;
        mLastKnownLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        if (isManualPolling && manualPollLocationListener != null) {
            //we don't really need to do anything here other than pass on the listener
            manualPollLocationListener.onLocationChanged(location);
        } else if (isAutoPolling) {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public boolean startManualPolling() {
        //we're manual polling, block all auto measurements.
        isManualPolling = true;
        if (!isUpdatingLocation)
            startLocationUpdates();
        return true;
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "You haven't granted this app permission to get location data", Toast.LENGTH_LONG);
            return;
        }

        lm = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        c.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        c.setAltitudeRequired(true);
        c.setSpeedRequired(true);
        c.setBearingRequired(true);

        //provider = lm.getBestProvider(c, true);
        lm.requestLocationUpdates(android.location.LocationManager.GPS_PROVIDER, 10L, 0.01f, this);
        lm.requestLocationUpdates(android.location.LocationManager.NETWORK_PROVIDER, 10L, 0.01f, this);

        //onLocationChanged(lm.getLastKnownLocation(provider));

        isUpdatingLocation = true;
        Log.d(TAG, "Location Manager: Location Updates Requested with provider " + provider);
    }

    protected void stopLocationUpdates() {
        lm = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //No permission to grab user location data, should probably warn the user.
            Toast.makeText(context, "You haven't granted this app permission to get location data", Toast.LENGTH_LONG);
            return;
        }
        lm.removeUpdates(this);
        isUpdatingLocation = false;
    }

    public void stopManualPolling(){
        //we're not manual polling anymore, we can continue auto measurements
        isManualPolling = false;
        stopLocationUpdates();
    }

    public boolean startAutoPolling(){
        //if manual polling then can't start auto polling
        if(isManualPolling==true)
            return false;
        isAutoPolling = true;
        if(!isUpdatingLocation)
            startLocationUpdates();
        return true;
    }

    public void stopAutoPolling(){
        isAutoPolling = false;
        stopLocationUpdates();
    }
}
