package com.bauerproductions.signalmapper.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Michael on 23/08/2016.
 */
public class NetworkManager extends BroadcastReceiver{

    public static NetworkManager mInstance;

    public Context context;
    ConnectivityManager connectivityManager;
    TelephonyManager telephonyManager;

    public Network_Type currentActiveNetwork = Network_Type.UNKNOWN;
    public boolean isWifiConnected;
    public boolean isGSMConnected;
    public boolean isCDMAConnected;
    public boolean isWCDMAConnected;
    public boolean isLTEConnected;

    public enum Network_Type{
        UNKNOWN(0),WIFI(1),GSM(1), CDMA(2), WCDMA(3), LTE(4);

        private int value;

        private Network_Type(int value){
            this.value = value;
        }
    }

    public NetworkManager(Context context){
        this.context = context;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public static NetworkManager getInstance(Context context){
        if(mInstance==null){
            mInstance = new NetworkManager(context);
        } else {
            mInstance.context = context;
        }
        return mInstance;
    }

    @Override
    public void onReceive(Context context,Intent intent){
        currentActiveNetwork = getActiveNetworkType();
        isWifiConnected = hasWIFIConnection();
        isGSMConnected = hasMobileConnection() && getTelephonyConnection()==Network_Type.GSM;
        isCDMAConnected = hasMobileConnection() && getTelephonyConnection()==Network_Type.CDMA;
        isWCDMAConnected = hasMobileConnection() && getTelephonyConnection()==Network_Type.WCDMA;
        isLTEConnected = hasMobileConnection() && getTelephonyConnection()==Network_Type.LTE;
    }

    public ArrayList<HashMap<String,String>> pollAllWIFINetworks(){
        ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> map;
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> scanResults = wifiManager.getScanResults();
        for(ScanResult result : scanResults){
            map = new HashMap<String,String>();
            map.put("level",String.valueOf(result.level));
            map.put("bssid",result.BSSID);
            map.put("ssid",result.SSID);
            map.put("timestamp",String.valueOf(result.timestamp));
            list.add(map);
        }
        return list;
    }

    public ArrayList<HashMap<String,String>> pollAllMobileNetworks(){
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //No permission to grab user location data, should probably warn the user.
            Toast.makeText(context, "You haven't granted this app permission to get location data", Toast.LENGTH_LONG);
            return null;

        }
        ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> map;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        List<CellInfo> scanResults = telephonyManager.getAllCellInfo();
        for(CellInfo result: scanResults){
            map = new HashMap<String,String>();
            if(CellInfoCdma.class.isInstance(result)){
                //is 2g or 3g (not found in australia only supported because android does)
                CellIdentityCdma id = ((CellInfoCdma) result).getCellIdentity();
                CellSignalStrengthCdma str = ((CellInfoCdma) result).getCellSignalStrength();
                map.put("type",String.valueOf(Network_Type.CDMA));
                map.put("level",String.valueOf(str.getDbm()));
                map.put("id",id.getNetworkId()+":"+id.getSystemId()+":"+id.getBasestationId());
                map.put("longitude",String.valueOf(id.getLongitude()));
                map.put("latitude",String.valueOf(id.getLatitude()));
                list.add(map);
            } else if(CellInfoGsm.class.isInstance(result)){
                //is 2g network
                CellIdentityGsm id = ((CellInfoGsm) result).getCellIdentity();
                CellSignalStrengthGsm str = ((CellInfoGsm) result).getCellSignalStrength();
                map.put("type",String.valueOf(Network_Type.GSM));
                map.put("level",String.valueOf(str.getDbm()));
                map.put("id",id.getMcc()+":"+id.getMnc()+":"+id.getLac()+":"+id.getCid());
                list.add(map);
            } else if(CellInfoLte.class.isInstance(result)){
                //is 4g
                CellIdentityLte id = ((CellInfoLte) result).getCellIdentity();
                CellSignalStrengthLte str = ((CellInfoLte) result).getCellSignalStrength();
                map.put("type",String.valueOf(Network_Type.LTE));
                map.put("level",String.valueOf(str.getDbm()));
                map.put("id",id.getMcc()+":"+id.getMnc()+":"+id.getCi()+":"+id.getPci()+":"+id.getTac());
                list.add(map);
            } else if(CellInfoWcdma.class.isInstance(result)){
                //is 3g
                CellIdentityWcdma id = ((CellInfoWcdma) result).getCellIdentity();
                CellSignalStrengthWcdma str = ((CellInfoWcdma) result).getCellSignalStrength();
                map.put("type",String.valueOf(Network_Type.WCDMA));
                map.put("level",String.valueOf(str.getDbm()));
                map.put("id",id.getMcc()+":"+id.getMnc()+":"+id.getLac()+":"+id.getCid()+":"+id.getPsc());
                list.add(map);
            }
        }
        return list;
    }

    public Network_Type getActiveNetworkType(){
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        int type = networkInfo.getType();
        if(type==ConnectivityManager.TYPE_WIFI){
            return Network_Type.WIFI;
        } else if(type==ConnectivityManager.TYPE_MOBILE){
            return getTelephonyConnection();
        }
        return Network_Type.UNKNOWN;
    }

    public boolean hasWIFIConnection(){
        Network[] networkList = connectivityManager.getAllNetworks();
        if(networkList!=null){
            for(Network network : networkList){
                NetworkInfo info = connectivityManager.getNetworkInfo(network);
                if(info.getType()==ConnectivityManager.TYPE_WIFI){
                    return info.isConnectedOrConnecting();
                }
            }
        }
        return false;
    }

    public boolean hasMobileConnection(){
        Network[] networkList = connectivityManager.getAllNetworks();
        if(networkList!=null){
            for(Network network : networkList){
                NetworkInfo info = connectivityManager.getNetworkInfo(network);
                if(info.getType()==ConnectivityManager.TYPE_MOBILE){
                    return info.isConnectedOrConnecting();
                }
            }
        }
        return false;
    }

    //Not sure if needed anymore
    public Network_Type getTelephonyConnection() {
        int networkType = telephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return Network_Type.GSM;//general 2g
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return Network_Type.CDMA;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return Network_Type.WCDMA;//general 3g
            case TelephonyManager.NETWORK_TYPE_LTE:
                return Network_Type.LTE;
            default:
                return Network_Type.UNKNOWN;
        }
    }

    public void requestNetwork(Network_Type type){
        //TODO:
    }
}
