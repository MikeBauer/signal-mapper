package com.bauerproductions.signalmapper.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.bauerproductions.signalmapper.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Michael on 3/08/2016.
 */
public class JSONUtils {


    public static ArrayList<String> fromJSONListString(String json){
        if(json==null)
            return null;
        ArrayList<String> res = new ArrayList<String>();
        try {
            JSONArray arr = new JSONArray(json);
            for(int i=0;i<arr.length();i++){
                res.add(arr.getString(i));
            }
            return res;
        } catch (JSONException e) {
            Log.e(MainActivity.TAG,"String to JSON Exception: "+e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static void fromJSONStringAsync(String json,JSONUnPackerCallback callback){
        JSONUnPacker task = new JSONUnPacker(callback);
        task.execute(json);
    }

    public static void toJSONStringAsync(ArrayList<HashMap<String,String>> map,JSONPackerCallback callback){
        JSONPacker task = new JSONPacker(callback);
        task.execute(map);
    }

    public static ArrayList<HashMap<String,String>> fromJSONString(String json){
        if(json==null)
            return null;
        ArrayList<HashMap<String,String>> mapArray = new ArrayList<HashMap<String,String>>();
        HashMap<String,String> map;
        try {
            JSONArray arr = new JSONArray(json);
            for(int i=0;i<arr.length();i++){
                map = new HashMap<String,String>();
                JSONObject obj = arr.getJSONObject(i);
                Iterator<String> it = obj.keys();
                while (it.hasNext()) {
                    String n = it.next();
                    map.put(n, obj.getString(n));
                }
                mapArray.add(map);
            }
            return mapArray;
        } catch (JSONException e) {
            Log.e(MainActivity.TAG,"String to JSON Exception: "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }


    public static String toJSONString(ArrayList<HashMap<String,String>> paramList){
        JSONArray arr = new JSONArray();
        JSONObject obj;
        try {
            for(int i=0;i<paramList.size();i++) {
                obj = new JSONObject();
                for (Map.Entry<String, String> entry : paramList.get(i).entrySet()) {
                    obj.put(entry.getKey(), entry.getValue());
                }
                arr.put(obj);
            }
            return arr.toString();
        } catch (JSONException e) {
            Log.e(MainActivity.TAG,"JSON to String Exception: "+e.getMessage());
        } catch (Exception e){
            return null;
        }
        return null;
    }


    private static class JSONPacker extends AsyncTask<ArrayList<HashMap<String,String>>, Integer, String> {

        JSONPackerCallback callback;
        int percent = 0;

        public JSONPacker(JSONPackerCallback callback){
            this.callback = callback;
        }

        protected String doInBackground(ArrayList<HashMap<String,String>>... data) {

            if(data.length>0){
                ArrayList<HashMap<String,String>> paramList = data[0];

                JSONArray arr = new JSONArray();
                JSONObject obj;
                try {
                    int size = paramList.size();
                    for(int i=0;i<size;i++) {
                        obj = new JSONObject();
                        for (Map.Entry<String, String> entry : paramList.get(i).entrySet()) {
                            obj.put(entry.getKey(), entry.getValue());
                        }
                        arr.put(obj);
                        //Because it will be common to send hundreds or thousands of datapoints
                        //It's best not to have the progress called thousands of times. At most 100 times.
                        int percentNow = (int)((i/ (float)size)*100);
                        if(percentNow>percent){
                            percent = percentNow;
                            publishProgress(percent);
                        }
                    }
                    return arr.toString();
                } catch (JSONException e) {
                    Log.e(MainActivity.TAG,"JSON to String Exception: "+e.getMessage());
                } catch (Exception e){
                    return null;
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            callback.onProgress(values[0]);
        }

        protected void onPostExecute(String result) {
            callback.onFinish(result);
        }
    }

    private static class JSONUnPacker extends AsyncTask<String, Integer, ArrayList<HashMap<String,String>>> {

        JSONUnPackerCallback callback;
        int percent=0;

        public JSONUnPacker(JSONUnPackerCallback callback){
            this.callback = callback;
        }

        protected ArrayList<HashMap<String,String>> doInBackground(String... data) {

            if(data.length>0){
                String json = data[0];

                if(json==null)
                    return null;
                ArrayList<HashMap<String,String>> mapArray = new ArrayList<HashMap<String,String>>();
                HashMap<String,String> map;
                try {
                    JSONArray arr = new JSONArray(json);
                    int size = arr.length();
                    for(int i=0;i<size;i++){
                        map = new HashMap<String,String>();
                        JSONObject obj = arr.getJSONObject(i);
                        Iterator<String> it = obj.keys();
                        while (it.hasNext()) {
                            String n = it.next();
                            map.put(n, obj.getString(n));
                        }
                        mapArray.add(map);
                        //Because it will be common to send hundreds or thousands of datapoints
                        //It's best not to have the progress called thousands of times. At most 100 times.
                        int percentNow = (int)((i/ (float)size)*100);
                        if(percentNow>percent){
                            percent = percentNow;
                            publishProgress(percent);
                        }
                    }
                    return mapArray;
                } catch (JSONException e) {
                    Log.e(MainActivity.TAG,"String to JSON Exception: "+e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            callback.onProgress(values[0]);
        }

        protected void onPostExecute(ArrayList<HashMap<String,String>> result) {
            callback.onFinish(result);
        }
    }

    public interface JSONPackerCallback{
        void onFinish(String json);
        void onProgress(int percent);
    }

    public interface  JSONUnPackerCallback{
        void onFinish(ArrayList<HashMap<String,String>> map);
        void onProgress(int percent);
    }
}
