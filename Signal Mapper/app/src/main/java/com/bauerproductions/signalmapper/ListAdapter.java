package com.bauerproductions.signalmapper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Michael on 16/09/2016.
 */
public class ListAdapter extends ArrayAdapter<HashMap<String,String>>{

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, int resource, List<HashMap<String,String>> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_view_tags, null);
        }

        HashMap<String,String> p = getItem(position);

        if (p != null) {
            TextView main = (TextView) v.findViewById(R.id.textViewMain);
            TextView sub = (TextView) v.findViewById(R.id.textViewSub);

            if(main != null){
                main.setText(p.get("tag"));
            }

            if(sub != null){
                sub.setText(p.get("count"));
            }

        }

        return v;
    }
}
