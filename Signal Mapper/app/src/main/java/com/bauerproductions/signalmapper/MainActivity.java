package com.bauerproductions.signalmapper;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bauerproductions.signalmapper.utils.Cache;
import com.bauerproductions.signalmapper.utils.DataPoint;
import com.bauerproductions.signalmapper.utils.LocationManager;
import com.bauerproductions.signalmapper.utils.NetworkManager;
import com.bauerproductions.signalmapper.utils.WebAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AboutFragment.OnAboutFragmentInteractionListener,
        MainFragment.OnMainFragmentInteractionListener,
        ManualPollFragment.OnManualPollFragmentInteractionListener,
        CacheFragment.OnCacheFragmentInteractionListener,
        SettingsFragment.OnSettingsFragmentInteractionListener {

    public static final String TAG = "signalmapper";
    public static final String EXTRA_POLL_TAG = "com.bauerproductions.signalmapper.POLL_TAG";
    public static final String EXTRA_POLL_MODE = "com.bauerproductions.signalmapper.POLL_MODE";
    public static final String EXTRA_POLL_ACCURACY = "com.bauerproductions.signalmapper.POLL_ACCURACY";

    public static final int PERMISSIONS_REQUEST_CODE_ACCESS_SCANALLWIFI = 1;
    public static final int PERMISSIONS_REQUEST_CODE_ACCESS_GETLOCATION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Begin at the about page
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new AboutFragment())
                .commit();

        Cache.loadFromMemory(this);
    }

    @Override
    protected void onDestroy() {
        Cache.loadToMemory(this);
        super.onDestroy();
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //TODO: Nothing here, left as stub in case I wish to populate it. Remove if not.

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fmanager = getFragmentManager();

        if (id == R.id.nav_about) {
            fmanager.beginTransaction()
                    .replace(R.id.content_frame, new AboutFragment())
                    .addToBackStack(null)
                    .commit();
        } else if (id == R.id.nav_settings) {
            fmanager.beginTransaction()
                    .replace(R.id.content_frame, new SettingsFragment())
                    .addToBackStack(null)
                    .commit();
        } else if (id == R.id.nav_manual) {
            fmanager.beginTransaction()
                    .replace(R.id.content_frame, new ManualPollFragment())
                    .addToBackStack(null)
                    .commit();
        } else if (id == R.id.nav_cache) {
            fmanager.beginTransaction()
                    .replace(R.id.content_frame, new CacheFragment())
                    .addToBackStack(null)
                    .commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAutoChunkUpload(View view){
        if(view.getId()==R.id.toggleButtonAutoChunkUpload){
            Cache.autoChunkUpload = ((ToggleButton)view).isChecked();
        }
    }

    public void setURL(View view){
        WebAdapter webAdapter = WebAdapter.getInstance(this);
        String endpoint = ((EditText)findViewById(R.id.editTextURL)).getText().toString();
        webAdapter.setEndpoint(endpoint);
        Toast.makeText(this,"Set URL to: "+webAdapter.endpoint,Toast.LENGTH_SHORT).show();
    }

    public void setChunkSize(View view){
        String size = ((EditText)findViewById(R.id.editTextChunkSize)).getText().toString();
        try{
            Cache.chunkSize = Integer.parseInt(size);
        } catch (NumberFormatException e){
            Toast.makeText(this,"Unable to set Chunk Size to: "+size+"   , It must be a number!",Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this,"Set Chunk Size to: "+Cache.chunkSize,Toast.LENGTH_SHORT).show();
    }

    public void testSendJsonData(View view){
        final WebAdapter webAdapter = WebAdapter.getInstance(this);

        ArrayList<DataPoint> points = new ArrayList<DataPoint>();
        for(int i = 0; i<5;i++){
            points.add(new DataPoint(99,99,99,99,99,99,99,99,"","","","test"));
        }

        webAdapter.sendMany(points, new WebAdapter.QuickResponse() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(webAdapter.context,"Success: "+response,Toast.LENGTH_SHORT).show();
                TextView resp = (TextView) findViewById(R.id.textViewResponse);
                resp.setText(response);
            }

            @Override
            public void onFailure(String response) {
                Toast.makeText(webAdapter.context,"Failure: "+response,Toast.LENGTH_SHORT).show();
                TextView resp = (TextView) findViewById(R.id.textViewResponse);
                resp.setText(response);
            }
        },null);
    }

    public void testSendSingleData(View view){
        final WebAdapter webAdapter = WebAdapter.getInstance(this);

        DataPoint point = new DataPoint(99,99,99,99,99,99,99,99,"","","","test");
        webAdapter.sendSingleDataPoint(point, new WebAdapter.QuickResponse() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(webAdapter.context,"Success: "+response,Toast.LENGTH_SHORT).show();
                TextView resp = (TextView) findViewById(R.id.textViewResponse);
                resp.setText(response);
            }

            @Override
            public void onFailure(String response) {
                Toast.makeText(webAdapter.context,"Failure: "+response,Toast.LENGTH_SHORT).show();
                TextView resp = (TextView) findViewById(R.id.textViewResponse);
                resp.setText(response);
            }
        });
    }

    public void testGetLocationDetails(View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_GETLOCATION);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }else{
            LocationManager locationManager = LocationManager.getInstance(this);
            Location loc = locationManager.getCurrentLocation();
            String result = "Null Location";
            if(loc!=null){
                result ="Latitude:     "+loc.getLatitude()+"\n"+
                        "Longitude:    "+loc.getLongitude()+"\n"+
                        "Accuracy:     "+loc.getAccuracy()+"\n"+
                        "Altitude:     "+loc.getAltitude()+"\n"+
                        "Bearing:     "+loc.getBearing()+"\n"+
                        "Speed:     "+loc.getSpeed()+"\n"+
                        "Last Updated: "+loc.getTime();
            }

            TextView resp = (TextView) findViewById(R.id.textViewResponse);
            resp.setText(result);
        }
    }

    public void testScanAllMobilePoints(View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_SCANALLWIFI);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }else{
            NetworkManager networkManager = NetworkManager.getInstance(this);

            String result = "---------\n";
            ArrayList<HashMap<String,String>> res = networkManager.pollAllMobileNetworks();

            for(HashMap<String,String> map : res){
                Iterator it = map.entrySet().iterator();
                while(it.hasNext()){
                    Map.Entry<String,String> pair = (Map.Entry<String,String>) it.next();
                    result = result+pair.getKey()+" \t\t "+pair.getValue()+"\n";
                    it.remove();
                }
                result = result + "---------\n";
            }

            TextView resp = (TextView) findViewById(R.id.textViewResponse);
            resp.setText(result);
        }
    }

    public void testScanAllWifiPoints(View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_SCANALLWIFI);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }else{
            NetworkManager networkManager = NetworkManager.getInstance(this);

            String result = "---------\n";
            ArrayList<HashMap<String,String>> res = networkManager.pollAllWIFINetworks();

            for(HashMap<String,String> map : res){
                Iterator it = map.entrySet().iterator();
                while(it.hasNext()){
                    Map.Entry<String,String> pair = (Map.Entry<String,String>) it.next();
                    result = result+pair.getKey()+" \t\t "+pair.getValue()+"\n";
                    it.remove();
                }
                result = result + "---------\n";
            }

            TextView resp = (TextView) findViewById(R.id.textViewResponse);
            resp.setText(result);
        }
    }

    @Override
    public void onAboutFragmentInteraction(Uri uri) {
        //TODO : Complete stub
    }

    @Override
    public void onMainFragmentInteraction(Uri uri) {
        //TODO : Complete stub
    }

    @Override
    public void onManualPollFragmentInteraction(View view) {
        //TODO : Complete stub
    }

    @Override
    public void onSettingsFragmentInteraction(Uri uri) {
        //TODO : Complete stub
    }

    @Override
    public void onCacheFragmentInteraction(Uri uri) {
        //TODO : Complete stub
    }
}
