'''
Created on 18 Oct 2016

@author: Michael
'''
import json, os, time, sys, traceback
from app.Models import DataPoint

run_path = os.path.dirname(os.path.realpath(__file__))
file_dest = os.path.join(run_path,'upload-backup')
cache_loc = os.path.join(run_path,'cache')

def process_file(file_str):
    
    with open(file_str,'r') as file:
        
        data = json.load(file)
        
        result = []
    
        for i in range(len(data)):
            try:
                datapoint = DataPoint(timestamp=data[i]['timestamp'],
                                     longitude=data[i]['longitude'],
                                     latitude=data[i]['latitude'],
                                     accuracy=data[i]['accuracy'],
                                     altitude=data[i]['altitude'],
                                     speed=data[i]['speed'],
                                     bearing=data[i]['bearing'],
                                     network_type=data[i]['network_type'],
                                     network_strength=data[i]['network_strength'],
                                     ssid=data[i]['ssid'],
                                     bssid=data[i]['bssid'],
                                     cid=data[i]['cid'],
                                     tag=data[i]['tag'])
                if not datapoint.addThis():
                    result.append({'index':str(i),'error':'not added'})
            except KeyError:
                result.append({'index':str(i),'error':'keyerror'})
        
        if len(result)>0:
            with open(file_str[:-4]+'log') as log:
                log.write(json.dumps(result))
                
        os.rename(file_str, os.path.join(file_dest,file_str.split('/')[-1]))

def process_cache():
    for root,dir,files in os.walk(cache_loc):
        for file in files:
            if '.json' in file:
                file_str = os.path.join(run_path,'cache',file)
                print "Processing: ",file_str
                process_file(file_str)

if __name__ == '__main__':
    print "Starting to Process Cached Files: ",time.strftime('%Y-%m-%d %H:%M:%S')

    try:
        process_cache()
        print "Process Successful"
    except:
        print "Process Failed Error:"
        err = []
        for i in sys.exc_info():
            err.append(str(i))
        print "\n".join(err)
        traceback.print_exc()
        
        