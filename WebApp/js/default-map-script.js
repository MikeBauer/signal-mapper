savedScript = function(data,map){
	for(var i in data){
		markers.push(new google.maps.Circle({
		  path: google.maps.SymbolPath.CIRCLE,
		  fillColor: 'red',
		  fillOpacity: 0.01,
		  center: {lat: data[i].latitude, lng: data[i].longitude},
		  radius: data[i].accuracy,
		  strokeColor: 'white',
		  strokeWeight: .5,
		  map: map
		}));
	}
}