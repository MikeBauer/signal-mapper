var editor = ace.edit("editor");
editor.setTheme("ace/theme/chrome");
var JavaScriptMode = ace.require("ace/mode/javascript").Mode;
editor.session.setMode(new JavaScriptMode());
editor.$blockScrolling = Infinity;

$(document).ready(function(){
	
	$('[name="save"]').click(function(){
		$("#response").html('<p>Saving Script...<p>');
		$.ajax(
			{
			url: "/editor/save", 
			async: true, 
			type: "POST",
			data: {
		    		context: "maps",
		        	name: $('[name="name"]').val(),
		        	content: editor.getValue()
		    	},
			success: function(result){
					$("#response").html('<p>'+result+'</p>');
					$.ajax({url: "/editor/filelist?context=maps", async: true, success: function(result){
						$('[name="filelist"]').html(result);
					}});
					$('[name="filelist"]').html('<option value="">Loading Files...</option>\n');
				}
			}
		);
	});
	
	$('[name="load"]').click(function(){
		$("#response").html('<p>Loading Script...<p>');
		$.ajax({
			url: "/usr/maps/"+$('[name=filelist]').val()+"?version="+Math.floor(Math.random()*1000), 
			async: true, 
			dataType: "text",
			success: function(result){
				editor.setValue(result);
				$('[name="name"]').val($('[name=filelist]').val());
				$("#response").html('');
			},
			error: function(xhr){
	            editor.setValue(xhr.responseText);
				$('[name="name"]').val($('[name=filelist]').val());
				$("#response").html('<p>'+"An error occured: " + xhr.status + " " + xhr.statusText+'</p>');
				
	        }
		});
	});
	
	$.ajax({url: "/editor/filelist?context=maps", async: true, success: function(result){
		$('[name="filelist"]').html(result);
	}});
	$('[name="filelist"]').html('<option value="">Loading Files...</option>\n');
			
});