<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Club Manager Lite">
<meta name="author" content="Bauer Productions">
<title>Signal Mapper</title>
<!-- Bootstrap core CSS -->
<link href="/styles/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="/styles/bootstrap-theme.min.css" rel="stylesheet">
<!-- Our Styles -->
<link rel="stylesheet" type="text/css" href="/styles/style.css">
<!-- Our Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
