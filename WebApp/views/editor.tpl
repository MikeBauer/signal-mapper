<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
	<style>
		#editor {
	        width: 100%;
	        height: 500px;
	    }
	</style>
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="editor")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">

<div class="form">

<table>
	<tr>
		<td>
			<input class="form-control" type="text" name="name" value="my_script">
		</td>
		<td>
			<button class="btn btn-default" name="save" value="save">Save Script</button>
		</td>
	</tr>
	<tr>
		<td>
			<div class="form-group">
				<select class="form-control" name="filelist"></select>
			</div>
		</td>
		<td>
			<button class="btn btn-default" name="load" value="load">Load Script</button>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="response"></div>
		</td>
	</tr>
</table>


</div>

<br>

<div id="editor"></div>
    

</div>
</div>

<script src="js/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="js/theme-chrome.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mode-javascript.js" type="text/javascript" charset="utf-8"></script>
<script src="js/editor.js" type="text/javascript" charset="utf-8"></script>

	% include('bsfoot.tpl')
</body>

</html>