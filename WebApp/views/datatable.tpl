			<div class="rTableRow">
				<div class="rTableHead">Type</div>
				<div class="rTableHead">Strength</div>
				<div class="rTableHead">Lng</div>
				<div class="rTableHead">Lat</div>
				<div class="rTableHead">Acc</div>
				<div class="rTableHead">BSSID</div>
				<div class="rTableHead">SSID</div>
				<div class="rTableHead">CID</div>
				<div class="rTableHead">TimeStamp</div>
			</div>
%count = 0
%for row in resultsList:
			<input name="selected" class="hideme" type="radio" id="{{row.id}}" name="res" value="{{row.id}}" />
				<label class="rTableRow" for="{{row.id}}">
					<div class="rTableCellLeft">{{row.network_type}}</div>
					<div class="rTableCell">{{row.network_strength}}</div>
					<div class="rTableCell">{{row.longitude}}</div>
					<div class="rTableCell">{{row.latitude}}</div>
					<div class="rTableCell">{{row.accuracy}}</div>
					<div class="rTableCell">{{row.bssid}}</div>
					<div class="rTableCell">{{row.ssid}}</div>
					<div class="rTableCell">{{row.cid}}</div>
					<div class="rTableCellRight">{{row.timestamp}}</div>
				</label>
%count = count+1
%if count>=limit:
%break
%end
%end