<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
	
	<style>
       #map {
        height: 600px;
        width: 100%;
       }
    </style>
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="maps")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">

<h4>Google Maps Data</h4>

<table>
	<tr>
		<td>
			<div class="form-group">
				<select class="form-control" name="filelist"></select>
			</div>
		</td>
		<td>
			<button class="btn btn-default" name="load" value="load">Use Script</button>
		</td>
	</tr>
</table>

<div id="response"></div>


<div id="map"></div>

</div>

</div>


<script id="mapsScript" src="js/{{script if len(script)>0 else 'default-map-script.js'}}" type="text/javascript" charset="utf-8"></script>
    <script>
    	var map;
    	var markers = [];
    	
    	function clearMap(){
    		while(markers[0])
    		{
    			markers.pop().setMap(null);
    		}
    	}
    	
    	function loadData(){
	    	$("#response").html('<p>Loading Data Overlay...<p>');
			$.getJSON('/api/json?cache=True', function(data) {
				savedScript(data,map);
				$("#response").html('<p><p>');
			});
	    }
    	
	    function initMap() {
	        var mapOptions = {
	          zoom: 16,
	          center: {lat: -33.774850301047, lng: 151.115631088614},
	          mapTypeId: 'terrain'
	        };
	
	        map = new google.maps.Map(document.getElementById('map'),mapOptions);

			google.maps.visualRefresh = true;
			loadData();
	     }
	    
	    
	    $(document).ready(function(){
	    	
	    	$('[name="load"]').click(function(){
	    		//window.location.href = '/maps?script='+$('[name=filelist]').val();
	    		$("#response").html('<p>Loading Script...<p>');
	    		$.ajax({
	    			url: "/usr/maps/"+$('[name=filelist]').val()+"?version="+Math.floor(Math.random()*1000), 
	    			async: true, 
	    			dataType: "script",
	    			success: function(result){
	    				//We need to clear all overlays to free memory first
	    				clearMap();
	    				
	    				$('[name="name"]').val($('[name=filelist]').val());
	    				$("#mapsScript").html(result);
	    				$("#response").html('<p><p>');
	    				loadData();
	    			}
	    		});
	    	});
	    	
		    $.ajax({url: "/editor/filelist?context=maps", async: true, success: function(result){
				$('[name="filelist"]').html(result);
			}});
			$('[name="filelist"]').html('<option value="">Loading Files...</option>\n');
	    });
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{mapKey}}&callback=initMap&libraries=visualization">
    </script>


	% include('bsfoot.tpl')
</body>

</html>