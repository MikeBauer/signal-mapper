<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="charts")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">

<h4>This page will contain links to a number of basic charts on the selected data</h4>
<h4>This page will also contain basic statistics on the selected data</h4>
<h4>This is still a work in Progress</h4>

</div>


	% include('bsfoot.tpl')
</body>

</html>