<nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Signal Mapper</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              {{!'<li class="active"><a href="/search">Search</a></li>' if title=='search' else '<li><a href="/search">Search</a></li>'}}
              {{!'<li class="active"><a href="/maps">Maps</a></li>' if title=='maps' else '<li><a href="/maps">Maps</a></li>'}}
              {{!'<li class="active"><a href="/editor">Script Editor</a></li>' if title=='editor' else '<li><a href="/editor">Script Editor</a></li>'}}
            </ul>
          </div>
        </div>
      </nav>
