<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="data")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">

<h3>Here is a list of all the data</h3>

<div class="rTable">
	
		<div class="rTableBody">
		
			<div class="rTableRow">
				<div class="rTableHead">Id</div>
				<div class="rTableHead">Lng</div>
				<div class="rTableHead">Lat</div>
				<div class="rTableHead">Acc</div>
				<div class="rTableHead">Type</div>
				<div class="rTableHead">Strength</div>
				<div class="rTableHead">BSSID</div>
				<div class="rTableHead">SSID</div>
				<div class="rTableHead">CID</div>
				<div class="rTableHead">TimeStamp</div>
				<div class="rTableHead">Tag</div>
			</div>
		
%for row in resultsList:
			<input name="selected" class="hideme" type="radio" id="{{row.id}}" value="{{row.id}}" />
				<label class="rTableRow" for="{{row.id}}">
					<div class="rTableCellLeft"><b>{{row.id}}</b></div>
					<div class="rTableCell">{{row.longitude}}</div>
					<div class="rTableCell">{{row.latitude}}</div>
					<div class="rTableCell">{{row.accuracy}}</div>
					<div class="rTableCell">{{row.network_type}}</div>
					<div class="rTableCell">{{row.network_strength}}</div>
					<div class="rTableCell">{{row.bssid}}</div>
					<div class="rTableCell">{{row.ssid}}</div>
					<div class="rTableCell">{{row.cid}}</div>
					<div class="rTableCell">{{row.timestamp}}</div>
					<div class="rTableCellRight">{{row.tag}}</div>
				</label>
%end
		</div>
	</div>


</div>
</div>


	% include('bsfoot.tpl')
</body>

</html>