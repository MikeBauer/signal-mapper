<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="Home")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">
<h3>Welcome to the Signal Mapper Web Application</h3>

<h4>There is currently {{count}} Data Points in the database!</h4>

<p>This application can show you exactly that. 
You manually survey an area of choice to determine the best location for your Wifi router. 
You can also leave it on to automatically survey areas of your choice as you pass through. 
Afterwards you can explore your results and satisfy your curiosity. </p>

<p>This application is designed by Michael Bauer as part of a study relating to the handover 
between networks on mobile devices. The aim is to find ways handover can be optimised such that 
it has as little impact on the quality during VOIP calls, video chat and any other streaming services.</p>

</div>
</div>


	% include('bsfoot.tpl')
</body>

</html>