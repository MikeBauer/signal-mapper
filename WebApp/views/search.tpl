<!DOCTYPE html>
<html>
<head>
	% include('bshead.tpl')
	
	<script>
		var tag="";
		var network_type="";
		var ssid="";
		var bssid="";
		var cid=""
		
		function call(){
			query = "";
			if(tag.length>0){
				query = query + "tag="+tag+"&";
			}
			if(network_type.length>0){
				query = query + "network_type="+network_type+"&";
			}
			if(ssid.length>0){
				query = query + "ssid="+ssid+"&";
			}
			if(bssid.length>0){
				query = query + "bssid="+bssid+"&";
			}
			if(cid.length>0){
				query = query + "cid="+cid+"&";
			}
			
			url = "/data";
			if(query.length>0){
				url = url+"?"+query.slice(0, -1);
			}
			$.ajax({url: url, async: true, success: function(result){
				$('[name="table"]').html(result);
				$('[name="message"]').html('');
			}});
			$('[name="message"]').html('<p>Loading Results...<p>');
		}
		
		$(document).ready(function(){
			$('[name="tag"]').click(function(event){
				if($(this).attr("id")=="alltag"){
					tag="";
				} else {
					tag=$(this).attr("id");
				}
				call();
			});
			$('[name="network_type"]').click(function(event){
				if($(this).attr("id")=="allnetwork_type"){
					network_type="";
				} else {
					network_type=$(this).attr("id");
				}
				call();
			});
			$('[name="ssid"]').click(function(event){
				if($(this).attr("id")=="allssid"){
					ssid="";
				} else {
					ssid=$(this).attr("id");
				}
				call();
			});
			$('[name="bssid"]').click(function(event){
				if($(this).attr("id")=="allbssid"){
					bssid="";
				} else {
					bssid=$(this).attr("id");
				}
				call();
			});
			$('[name="cid"]').click(function(event){
				if($(this).attr("id")=="allcid"){
					cid="";
				} else {
					cid=$(this).attr("id");
				}
				call();
			});
		});
	</script>
	
</head>

<body>

<div class="navi">
	% include('nav.tpl',title="search")
</div>

<div class="content">
	%if len(message)>0:
	<div class="alert alert-warning" role="alert">
		<p align="center"><b>{{message}}</b></p>
	</div>
	%end
<div align="center">

<h3>Select an Experiment to view data</h3>

<div style="float:left;width:15%;">

	<div class="rTable">

		<div class="rTableRow">
			<div class="rTableHead">Tag</div>
			<div class="rTableHead">Count</div>
		</div>
		
		<div class="rTableBody">
		
			<input name="selected" class="hideme" type="radio" id="alltag" name="rtag" value="Get All" />
				<label class="rTableRow" name="tag" id="alltag" for="alltag">
					<div class="rTableCellLeft"><b>All Tags</b></div>
					<div class="rTableCellRight"><b>{{count}}</b></div>
				</label>
		
%for tag in metadata['tag']:
			<input name="selected" class="hideme" type="radio" id="{{tag['field']}}"  name="rtag" value={{tag['field']}}" />
				<label class="rTableRow" name="tag" id="{{tag['field']}}" for="{{tag['field']}}">
					<div class="rTableCellLeft">{{tag['field']}}</div>
					<div class="rTableCellRight">{{tag['count']}}</div>
				</label>
%end

		</div>
	</div>
		
	<div class="rTable">
		<div class="rTableRow">
			<div class="rTableHead">Network Type</div>
			<div class="rTableHead">Count</div>
		</div>
		
		<input name="selected" class="hideme" type="radio" id="allnetwork_type" name="rnetwork_type" value="Get All" />
				<label class="rTableRow" name="network_type" id="allnetwork_type" for="allnetwork_type">
					<div class="rTableCellLeft"><b>All Networks</b></div>
					<div class="rTableCellRight"><b>{{count}}</b></div>
				</label>
		
		<div class="rTableBody">
%types = ['UNKNOWN','WIFI','GSM','CDMA','WCDMA','LTE']
%for tag in metadata['network_type']:
			<input name="selected" class="hideme" type="radio" id="{{tag['field']}}" name="rnetwork_type" value="{{tag['field']}}" />
				<label class="rTableRow" name="network_type" id="{{tag['field']}}" for="{{tag['field']}}">
					<div class="rTableCellLeft">
					% if int(tag['field']) < len(types):
					{{types[int(tag['field'])]}}
					% else:
					{{types[0]}}
					%end
					</div>
					<div class="rTableCellRight">{{tag['count']}}</div>
				</label>
%end
		
		</div>
		
	</div>

	<div class="rTable">

		<div class="rTableRow">
			<div class="rTableHead">SSID</div>
			<div class="rTableHead">Count</div>
		</div>
		
		<div class="rTableBody">
		
			<input name="selected" class="hideme" type="radio" id="allssid" name="rssid" value="Get All" />
				<label class="rTableRow" name="ssid" id="allssid" for="allssid">
					<div class="rTableCellLeft"><b>All SSIDs</b></div>
					<div class="rTableCellRight"><b>{{count}}</b></div>
				</label>
		
%for tag in metadata['ssid']:
			<input name="selected" class="hideme" type="radio" id="{{tag['field']}}" name="rssid" value="{{tag['field']}}" />
				<label class="rTableRow" name="ssid" id="{{tag['field']}}" for="{{tag['field']}}">
					<div class="rTableCellLeft">{{tag['field']}}</div>
					<div class="rTableCellRight">{{tag['count']}}</div>
				</label>
%end

		</div>
	</div>
	
	<div class="rTable">

		<div class="rTableRow">
			<div class="rTableHead">BSSID</div>
			<div class="rTableHead">Count</div>
		</div>
		
		<div class="rTableBody">
		
			<input name="selected" class="hideme" type="radio" id="allbssid" name="rbssid" value="Get All" />
				<label class="rTableRow" name="bssid" id="allbssid" for="allbssid">
					<div class="rTableCellLeft"><b>All BSSIDs</b></div>
					<div class="rTableCellRight"><b>{{count}}</b></div>
				</label>
		
%for tag in metadata['bssid']:
			<input name="selected" class="hideme" type="radio" id="{{tag['field']}}" name="rbssid" value="{{tag['field']}}" />
				<label class="rTableRow" name="bssid" id="{{tag['field']}}" for="{{tag['field']}}">
					<div class="rTableCellLeft">{{tag['field']}}</div>
					<div class="rTableCellRight">{{tag['count']}}</div>
				</label>
%end

		</div>
	</div>
	
	
	<div class="rTable">

		<div class="rTableRow">
			<div class="rTableHead">CID</div>
			<div class="rTableHead">Count</div>
		</div>
		
		<div class="rTableBody">
		
			<input name="selected" class="hideme" type="radio" id="allcid" name="rcid" value="Get All" />
				<label class="rTableRow" name="cid" id="allcid" for="allcid">
					<div class="rTableCellLeft"><b>All CIDs</b></div>
					<div class="rTableCellRight"><b>{{count}}</b></div>
				</label>
		
%for tag in metadata['cid']:
			<input name="selected" class="hideme" type="radio" id="{{tag['field']}}" name="rcid" value="{{tag['field']}}" />
				<label class="rTableRow" name="cid" id="{{tag['field']}}" for="{{tag['field']}}">
					<div class="rTableCellLeft">{{tag['field']}}</div>
					<div class="rTableCellRight">{{tag['count']}}</div>
				</label>
%end

		</div>
	</div>

</div>

</div>

<div style="float:right;width:80%;">

<form action="/" method="GET">
<div class="rTable">
	
		<div class="rTableBody" name="table">
		
			<div class="rTableRow">
				<div class="rTableHead">Type</div>
				<div class="rTableHead">Strength</div>
				<div class="rTableHead">Lng</div>
				<div class="rTableHead">Lat</div>
				<div class="rTableHead">Acc</div>
				<div class="rTableHead">BSSID</div>
				<div class="rTableHead">SSID</div>
				<div class="rTableHead">CID</div>
				<div class="rTableHead">TimeStamp</div>
			</div>
			
			<div name="message">
			</div>
			
		</div>
	</div>
</div>
</form>

</div>

</div>

	% include('bsfoot.tpl')
</body>

</html>