'''
Created on 17 Oct 2016

@author: Michael
'''

import csv, os, time, sys, traceback
from app.Models import DataPoint

run_path = os.path.dirname(os.path.realpath(__file__))

def write_csv():

    resultsList = DataPoint.SELECT({})
    data = []
    for dp in resultsList:
        data.append(dp.__dict__)

    with open(os.path.join(run_path,'backup',time.strftime('%Y-%m-%d')), 'wb') as csvfile:
        dict_writer = csv.DictWriter(csvfile,data[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(data)

if __name__ == '__main__':

    print "Starting backup: ",time.strftime('%Y-%m-%d %H:%M:%S')


    try:
        write_csv()
        print "Backup Successful"
    except:
        print "Backup Failed Error:"
        err = []
        for i in sys.exc_info():
            err.append(str(i))
        print "\n".join(err)
        traceback.print_exc()