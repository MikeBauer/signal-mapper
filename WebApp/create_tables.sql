CREATE TABLE IF NOT EXISTS signal_data (
	id			SERIAL		PRIMARY KEY,
	timestamp   DATETIME,
	longitude	DOUBLE,
	latitude	DOUBLE,
	accuracy	DOUBLE,
	altitude	DOUBLE,
	bearing	DOUBLE,
	speed	DOUBLE,
	network_type INT,
	network_strength INT,
	ssid		TEXT,
	bssid		TEXT,
	cid			TEXT,
	tag			TEXT
);