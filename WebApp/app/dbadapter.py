'''
Created on 15 Aug 2016

@author: Michael

@summary: A Mysql Adapter for the Signal Mapper Webapp
'''
import MySQLdb
import datetime,bleach
import unicodedata

testConfig = {
              'host':'127.0.0.1',
              'user':'mikebauer',
              'passwd':'firefly!',
              'db':'mapdb'
              }

config = {
          'host':'mikebauer.mysql.pythonanywhere-services.com',
          'user':'mikebauer',
          'passwd':'firefly!',
          'db':'mikebauer$mapdb'
          }


debug = False

def CUSTOM_SELECT(statement,params=None):
    '''
        Allows more specific SELECT statement usage beyond what is supported with the SELECT function. 
        It is potentially vulnerable to SQLi attacks to be sure ALL user entered data is screened first.
        Returns a List of Dicts, each Dict a row and keys correspond to selected columns.
    '''
    #execute statement
    try:
        db = MySQLdb.connect(**config)
        cur = db.cursor()
        
        if params==None:
            if debug:
                print statement
            cur.execute(statement)
        else:
            if debug:
                print statement % tuple(params)
            cur.execute(statement,params)
        
        result = []
        
        for row in cur.fetchall():
            cols = []
            fixedRow = []
            for i in range(len(row)):
                if isinstance(row[i], datetime.datetime):
                    fixedRow.append(row[i].strftime('%Y-%m-%d %H:%M:%S'))
                else:
                    fixedRow.append(row[i])
            for i in cur.description:
                cols.append(i[0])
            result.append(dict(zip(cols,fixedRow)))
        
        db.close()
        
        return result
    except Exception as err:
        print "Exception: ",err
        return []

def SELECT(table,filters,select="*",orderby='',ascending=True,limit=-1):
    '''
        A Generic SQL SELECT statement from 'table' with keys matching columns and their desired filter
        returning the columns specified in 'select' (list of strings). 'select' can also be just a string
        that is meant to represent that part of the statement (even '*' is possible and is the default)
    '''    
    #create generic statement
    #Create SELECT and FROM components
    if isinstance(select, str):
        statement = 'SELECT '+select+' FROM '+table
    else:
        statement = 'SELECT '
        for sel in select:
            if sel != None:
                statement = statement+sel+','
        statement = statement[:-1] + ' FROM '+table
    #Create WHERE component
    vals = []
    if len(filters)>0:
        statement = statement+' WHERE '
        for key,val in filters.items():
            if val != None:
                if isinstance(val, list):
                    ors = []
                    for v in val:
                        ors.append(key+'=%s')
                        vals.append(v)
                    statement = statement + '(' + ' or '.join(ors) + ') and '
                else:
                    statement = statement+key+'=%s and '
                    vals.append(val)
        statement = statement[:-5]
    #Create Order By component
    if len(orderby)>0:
        #The 'order by' here requires no quotations, 
        #as MySQLdb stupidly adds them for my convenience
        #I must validate 'orderby' myself
        orderby = bleach.clean(orderby)
        #Bleach outputs unicode and I don't want that
        orderby = unicodedata.normalize('NFKD', orderby).encode('ascii','ignore')
        if not ';' in orderby:
            #As it's far enough through the current statement
            #making sure this isn't terminated and another started
            #prevents any actual information from being leaked. 
            statement = statement+" ORDER BY "+orderby
            if ascending:
                statement = statement+" ASC"
            else:
                statement = statement+" DESC"
    #Create LIMIT component
    if limit>0:
        statement = statement+" LIMIT "+str(limit)
    
    if debug:
        print statement % tuple(vals)
    #execute statement
    try:
        db = MySQLdb.connect(**config)
        cur = db.cursor()
        
        cur.execute(statement,tuple(vals))
        
        result = []
        
        for row in cur.fetchall():
            cols = []
            fixedRow = []
            for i in range(len(row)):
                if isinstance(row[i], datetime.datetime):
                    fixedRow.append(row[i].strftime('%Y-%m-%d %H:%M:%S'))
                else:
                    fixedRow.append(row[i])
            for i in cur.description:
                cols.append(i[0])
            result.append(dict(zip(cols,fixedRow)))
        
        db.close()
        
        return result
    except Exception as err:
        print "Exception: ",err
        return []

def DELETE(table,filters):
    '''
        A Generic SQL DELETE statement into 'table' with keys matching columns and their desired filters
    '''
    #Considering whether it's needed at all, nothing should ever be deleted except for test data
    return None

def INSERT(table,values):
    '''
        A Generic SQL INSERT statement into 'table' with keys matching columns and their desired values
    '''
    #clean the dirty dirty data
    for key in values.keys():
        if isinstance(values[key],str):
            values[key] = bleach.clean(values[key])
    
    #create generic statement
    statement = 'INSERT INTO '+table+' ('
    vals = []
    count = 0
    for key,val in values.items():
        if val != None:
            statement = statement+''+key+','
            vals.append(val)
            count += 1
    statement = statement[:-1]+') VALUES ('
    for i in range(count):
        statement = statement+'%s,'
    statement = statement[:-1]+')'
    
    if debug:
        print statement % tuple(vals)
    #execute statement
    try:
        db = MySQLdb.connect(**config)
        cur = db.cursor()
        
        cur.execute(statement,tuple(vals))
        
        rowcount = cur.rowcount
        db.commit()
        db.close()
        return rowcount 
    except Exception as err:
        print "Exception: ",err
        return 0
    
def UPDATE(table,filters,values):
    #clean the dirty dirty data
    for key in values.keys():
        if isinstance(values[key],str):
            values[key] = bleach.clean(values[key])
    
    #create generic statement
    statement = 'UPDATE members SET '
    vals = []
    for key,val in values.items():
        if val != None:
            if val.startswith(key):
                #be careful here, val isn't necessarily sanitized
                statement = statement+key+'='+val+','
            else:
                statement = statement+key+'=%s,'
                #Because mysql hates booleans
                if val.upper()=="TRUE":
                    vals.append("1")
                elif val.upper()=="FALSE":
                    vals.append("0")
                else:
                    vals.append(val)
    statement = statement[:-1]+' WHERE '
    for key,val in filters.items():
        if val != None:
            statement = statement+key+'=%s,'
            vals.append(val)
    statement = statement[:-1]
    
    if debug:
        print statement % tuple(vals)
    #execute statement
    try:
        db = MySQLdb.connect(**config)
        cur = db.cursor()
        
        cur.execute(statement,tuple(vals))
        
        rowcount = cur.rowcount
        db.commit()
        db.close()
        return rowcount 
    except Exception as err:
        print "Exception: ",err
        return 0


        
if __name__ == '__main__':
    
    config = testConfig
    debug = True
    
    SELECT('example_table', {'key1':'value1','key2':['value21','value22']}, select=['select1','select2'], orderby='select1', ascending=True)
    
    
    #now test the SQL functions (todo: make unit tests)