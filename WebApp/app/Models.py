'''
Created on 8 Sep 2016

@author: Michael
'''
import time,sys
import dbadapter as db

class DataPoint(object):
    
    type = ["UNKNOWN","WIFI","GSM","CDMA","WCDMA","LTE"]
    
    def __init__(self, id=-1, timestamp=time.strftime('%Y-%m-%d %H:%M:%S'),
                 longitude=0.0, latitude=0.0, accuracy=99999.9, network_type=-1, 
                 network_strength=1,altitude=0.0,speed=0.0,bearing=0.0, ssid="",bssid="",cid="", tag=""):
        #The 0.0 value in altitude,speed and baring are the values returned by android if none exist.
        self.timestamp = timestamp
        self.id = int(id)
        self.longitude = float(longitude)
        self.latitude = float(latitude)
        self.accuracy = float(accuracy)
        self.altitude = float(altitude)
        self.speed = float(speed)
        self.bearing = float(bearing)
        self.network_type = int(network_type)
        self.network_strength = int(network_strength)
        try:
            self.ssid = str(ssid)
        except UnicodeEncodeError:
            self.ssid = ssid.encode('ascii', errors='backslashreplace')
        self.bssid = str(bssid)
        self.cid = str(cid)
        self.tag = str(tag)
        
    def addThis(self):
        d = self.__dict__
        if self.id<0:
            del d['id']
        
        return db.INSERT("signal_data", d) > 0

    @staticmethod
    def getWithTag(tag,limit=-1):
        res = db.SELECT("signal_data", {'tag':tag},orderby='id',limit=limit,ascending=False)
        datalist = []
        for values in res:
            datalist.append(DataPoint(**values))
        return datalist
    
    @staticmethod
    def SELECT(filters,select="*",orderby='',ascending=True,limit=-1,asdict=False):
        res = db.SELECT("signal_data", filters=filters, select=select, orderby=orderby, ascending=ascending, limit=limit)
        datalist = []
        if asdict:
            return res
        for values in res:
            datalist.append(DataPoint(**values))
        return datalist

    @staticmethod
    def getDataPointCount():
        res = db.CUSTOM_SELECT("SELECT COUNT(id) AS count FROM signal_data")
        return res[0]['count']

    @staticmethod
    def getFields(field,limit=-1):
        s = "SELECT "+field+" as field,COUNT(*) as count FROM signal_data GROUP BY field ORDER BY count DESC"
        if limit>0:
            s = s+" LIMIT %d"%limit
        return db.CUSTOM_SELECT(s)

    @staticmethod
    def getAllTags(limit=-1):
        s = "SELECT DISTINCT tag, COUNT(tag) AS count FROM signal_data GROUP BY tag"
        if limit>0:
            s = s+" LIMIT %d"%limit
        return db.CUSTOM_SELECT(s)

    @staticmethod
    def getAll(limit=-1):
        res = db.SELECT("signal_data", {},orderby='id',limit=limit,ascending=False)
        datalist = []
        for values in res:
            datalist.append(DataPoint(**values))
        return datalist
   
if __name__ == '__main__':
    
    db.config = db.testConfig
    
    #now test functions using this config
    print 'Testing Add to database'
    
    d = {
         'bearing': 0.0, 
         'ssid': 'Macquarie OneNet', 
         'bssid': '40:e3:d6:e9:22:10', 
         'cid': '', 
         'timestamp': '2016-09-13 14:26:53', 
         'altitude': 0.0, 
         'longitude': 151.11563108861446, 
         'network_type': 1, 
         'network_strength': -67, 
         'tag': 'test', 
         'latitude': -33.77485030104696, 
         'speed': 0.0, 
         'id': -1, 
         'accuracy': 0.0
         }
    
    sample = DataPoint(**d)
    if sample.addThis():
        print 'Success'
    else:
        print 'Fail'
    
    print 'Testing Get All Items (limit 5)'
    test = DataPoint.getAll(limit=5)
    for item in test:
        print item.id,item.timestamp,item.longitude,item.latitude,item.tag     