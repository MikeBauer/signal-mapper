'''
Created on 15 Aug 2016

@author: Michael
'''

from app import dbadapter
import sys,os,time
import bottle
import json,csv
from bottle import route,post,get
from beaker.middleware import SessionMiddleware
from app.Models import DataPoint
from io import BytesIO

mapsApiKey = "AIzaSyAX8eaN_NdRyQoP5Qo7j38UnxguN805TX8"

mapsTestApiKey = "AIzaSyCJ8XS-Gon5SZradFWRABm-QzkKKBRRrHA"

run_path = os.path.dirname(os.path.realpath(__file__))

session_opts = {
    'session.cookie_expires': False
}

bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024 * 16 #16mb
application = SessionMiddleware(bottle.app(), session_opts)

@route('/styles/<filename>')
def serve_style(filename):
    '''Loads static files from /styles. Store all .css files there.'''
    return bottle.static_file(filename, root='./styles')

@route('/js/<filename>')
def send_static(filename):
    '''Loads static files from /js. Store all .js files there.'''
    return bottle.static_file(filename, root='./js/')

@route('/usr/<context>/<filename>')
def send_user(context,filename):
    '''Loads static files from /usr/ Store all user uploaded files there.'''
    return bottle.static_file(os.path.join(context,filename), root='./usr/')

@route('/')
def home():
    '''An introductory home page to welcome the user and brief them on the process'''
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    
    return bottle.template('home', message='',count=DataPoint.getDataPointCount())

@get('/data')
def get_data():
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    
    query = dict(bottle.request.query)
    showlimit = query.get('showlimit', 100)
    limit = query.get('limit', -1)
    orderby = query.get('orderby', '')
    asc = query.get('asc', 'True') == 'True'
    if 'limit' in query:
        del query['limit']
    if 'showlimit' in query:
        del query['showlimit']
    if 'orderby' in query:
        del query['orderby']
    if 'asc' in query:
        del query['asc']
        
    try:
        resultsList = DataPoint.SELECT(query, orderby=orderby, ascending=asc, limit=limit)
        data = []
        for dp in resultsList:
            data.append(dp.__dict__)
        session['cache'] = data
        session.save()
    except:
        return ''
    
    return bottle.template('datatable', message='', resultsList=resultsList,limit=showlimit)


@post('/data')
def post_data():
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    
    query = dict(bottle.request.forms)
    showlimit = query.get('showlimit', 100)
    limit = query.get('limit', -1)
    orderby = query.get('orderby', '')
    asc = query.get('asc', 'True') == 'True'
    if 'limit' in query:
        del query['limit']
    if 'showlimit' in query:
        del query['showlimit']
    if 'orderby' in query:
        del query['orderby']
    if 'asc' in query:
        del query['asc']
        
    try:
        resultsList = DataPoint.SELECT(query, orderby=orderby, ascending=asc, limit=limit)
        data = []
        for dp in resultsList:
            data.append(dp.__dict__)
        session['cache'] = data
        session.save()
    except:
        return ''
    
    return bottle.template('datatable', message='', resultsList=resultsList,limit=showlimit)


@get('/search')
def search_data():
    
    fields = ['tag','network_type','ssid','bssid','cid']
    metadata = {}
    for i in fields:
        metadata[i] = DataPoint.getFields(i)
    
    return bottle.template('search', message='', 
                           count=DataPoint.getDataPointCount() , 
                           resultsList=DataPoint.getAll(limit=100), 
                           metadata=metadata)

@get('/search/data-with-tag')
def get_data_with_tag():
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    try:
        tag = dict(bottle.request.query)['tag']
        session['tag'] = tag
        session.save()
        return bottle.template('datatable', message='', resultsList=DataPoint.getWithTag(tag, limit=100))
    except KeyError:
        session['tag'] = ''
        session.save()
        return bottle.template('datatable', message='', resultsList=DataPoint.getAll(limit=100))

@route('/charts')
def get_charts():
    return bottle.template('charts', message='')

@route('/maps')
def get_maps():
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    
    script = bottle.request.query.get("script", "")
    
    return bottle.template('maps', message='', mapKey=mapsApiKey,script=script)

@route('/editor')
def get_editor():
    return bottle.template('editor', message='')

@get('/editor/filelist')
def get_filelist():
    
    context = bottle.request.query.get("context", "")
    path = 'usr'
    if len(context)>0:
        path = os.path.join(run_path,'usr',context)
    files = []
    for (dirpath, dirnames, filenames) in os.walk(path):
        files.extend(filenames)
        break
    
    return ''.join('<option value="%s">%s</option>\n' % (s,s) for s in files)

@post('/editor/save')
def save_file():
    
    res = bottle.request.forms.dict
    
    try:
        context = res['context'][0]
        name = res['name'][0]
        content = res['content'][0]
        
        if len(content)==0:
            return "Files not saved: Empty File."
        
        if len(name)==0:
            return "Files not saved: Empty File Name."
        
        if name[-3:]!='.js':
            name = name+'.js'
        
        with open(os.path.join(run_path,'usr',context,name),'w') as file:
            file.write(content)
        
    except KeyError:
        return 'File not saved.'

@post('/api/add')
def add_data():
    #will need to check an api but will do such a thing later
    res = bottle.request.forms.dict
    
    try:
        data = DataPoint(timestamp=res['timestamp'][0],
                                 longitude=res['longitude'][0],
                                 latitude=res['latitude'][0],
                                 accuracy=res['accuracy'][0],
                                 altitude=res['altitude'][0],
                                 speed=res['speed'][0],
                                 bearing=res['bearing'][0],
                                 network_type=res['network_type'][0],
                                 network_strength=res['network_strength'][0],
                                 ssid=res['ssid'][0],
                                 bssid=res['bssid'][0],
                                 cid=res['cid'][0],
                                 tag=res['tag'][0])
        if data.addThis():
            return 'success'
    except KeyError:
        return 'fail: keyerror'
    
    return 'fail: not added'

@get('/api/csv')
def get_csv():
    
    query = dict(bottle.request.query)
    showlimit = query.get('showlimit', 100)
    limit = query.get('limit', -1)
    orderby = query.get('orderby', '')
    asc = query.get('asc', 'True') == 'True'
    if 'limit' in query:
        del query['limit']
    if 'showlimit' in query:
        del query['showlimit']
    if 'orderby' in query:
        del query['orderby']
    if 'asc' in query:
        del query['asc']
        
    try:
        resultsList = DataPoint.SELECT(query, orderby=orderby, ascending=asc, limit=limit)
        data = []
        for dp in resultsList:
            data.append(dp.__dict__)
    except:
        return ''
    
    #make response header so that file will be downloaded.
    bottle.response.headers["Content-Disposition"] = "attachment; filename=data.csv"
    bottle.response.headers["Content-type"] = "text/csv"

    csvfile = BytesIO()
    dict_writer = csv.DictWriter(csvfile,data[0].keys())
    dict_writer.writeheader()
    dict_writer.writerows(data)

    csvfile.seek(0)
    return csvfile.read()
    

@get('/api/taglist')
@post('/api/taglist')
def get_tag_list():
    return json.dumps(DataPoint.getAllTags())
    

@post('/api/json')
def add_json():
    
    if bottle.request.content_type != "application/json":
        return "fail: Send JSON, You sent: "+bottle.request.content_type

    data = bottle.request.json
    
    timestamp = time.strftime('%Y-%m-%d-%H-%M-%S')
    try:
        with open(os.path.join(run_path,'cache','%s.json' % timestamp),'w') as file:
            file.write(json.dumps(data))
    except:
        return ''
    return json.dumps([{'success':'True'}])
    

@get('/api/json')
def get_json():
    session = bottle.request.environ.get('beaker.session')  #@UndefinedVariable
    
    useCache = bottle.request.query.get("cache", "False") == 'True'
    
    if useCache:
        try:
            data=session['cache']
        except KeyError:
            data=[]
        
    else:
        tag = bottle.request.query.get("tag", "")
        res = []
        if len(tag)>0:
            res = DataPoint.getWithTag(tag)
    
        data = []
        for dp in res:
            data.append(dp.__dict__)
        
    return json.dumps(data)


if __name__ == '__main__':
    print "Starting Signal Mapper Webapp..."
    dbadapter.config = dbadapter.testConfig
    dbadapter.debug = True
    mapsApiKey = mapsTestApiKey
    if len(sys.argv)>1:
        bottle.run(app=application, host=sys.argv[1], port=8000, debug=True)
    else:
        bottle.run(app=application, host='localhost', port=8080, debug=True)